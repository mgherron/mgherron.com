<?php
/**
 * Template Name: Homepage [ACF]
 * @package Make
 */

get_header();
global $post;
?>

<main id="site-main" class="site-main" role="main">

<!-- Hero Module -->
<?php
	$mh_heroTitle      = get_field('heroTitle');
	$mh_heroTitleColor = $mh_heroTitle['heroTitleColor'];

	$mh_heroBackground = get_field('heroBackground');
	$mh_heroBGColor    = $mh_heroBackground['heroBackgroundColor'];
	$mh_heroBGImage    = $mh_heroBackground['heroBackgroundImage'];
	$mh_heroBGStyle    = "background-color: " . $mh_heroBGColor;
	$mh_heroBGStyle    .= "; background-image: url(" . $mh_heroBGImage['url'] . ");";

	$mh_heroImage      = get_field('heroImage');
	$mh_heroImageFile  = $mh_heroImage['heroImageFile'];
?>
<section class="mh-module-hero" style="<?php echo $mh_heroBGStyle; ?>">
	<div class="mh-module-hero-text">
		<?php if( $mh_heroTitle ): ?>
			<h1 class="mh-module-hero-text-title" style="color: <?php echo $mh_heroTitleColor; ?>"><?php echo $mh_heroTitle['heroTitleText']; ?></h1>
		<?php endif; ?>
		<h2 class="mh-module-hero-text-subtitle"><?php the_field('heroSubtitle'); ?></h2>
		<div class="mh-module-hero-text-description">
			<?php the_field('heroDescription'); ?>
		</div>
		<?php $mh_heroButtonLink = get_field('heroButtonLink'); if( $mh_heroButtonLink ): ?>
			<a href="<?php echo $mh_heroButtonLink; ?>" <?php $mh_heroButtonLinkType = get_field('heroButtonLinkType'); if( $mh_heroButtonLinkType == 'external' ): ?> target="_blank" <?php endif ?> class="mh-button mh-module-hero-action" style="background-color: <?php echo $mh_heroTitleColor; ?>"><?php echo get_field('heroButtonText'); ?></a>
		<?php endif; ?>
	</div>
	<?php if( $mh_heroImageFile ): ?>
		<div class="mh-module-hero-image">
			<?php if( $mh_heroImage['heroImage3D']) { ?>
			<div class="bookcover">
				<div class="bookcover-inner">
					<?php echo wp_get_attachment_image( $mh_heroImageFile, 'large' ); ?>
				</div>
			</div>
			<?php } else { ?>
				<?php echo wp_get_attachment_image( $mh_heroImageFile, 'large' ); ?>
			<?php } ?>
		</div>
	<?php endif; ?>
</section>

<!-- Books to Promote Module -->
<?php
	$mh_otherbooksTitle    = get_field('booksPromoTitle');
	$mh_otherbooksSubtitle = get_field('booksPromoSubtitle');
?>
<?php if( have_rows('booksPromoBookSelect') ): ?>
	<section class="mh-module-otherbooks">
		<?php if( $mh_otherbooksTitle ): ?>
			<h2 class="mh-module-otherbooks-title"><?php echo $mh_otherbooksTitle; ?></h2>
		<?php endif; ?>
		<?php if( $mh_otherbooksSubtitle ): ?>
			<p class="mh-module-otherbooks-subtitle"><?php echo $mh_otherbooksSubtitle; ?></p>
		<?php endif; ?>
		<div class="mh-module-otherbooks-group">
			<?php while( have_rows('booksPromoBookSelect') ): the_row();
				$mh_bookTitle           = get_sub_field('bookTitle');
				$mh_bookDescription     = get_sub_field('bookDescription');
				$mh_bookImage           = get_sub_field('bookImage');
				$mh_bookButton          = get_sub_field('bookButton');
				$mh_bookButtonLabel     = $mh_bookButton['bookButtonLabel'];
				$mh_bookButtonLink      = $mh_bookButton['bookButtonLink'];
				$mh_bookButtonLinkType  = $mh_bookButton['bookButtonLinkType'];
				$mh_bookOptions         = get_sub_field('bookOptions');
			?>
			<?php if( $mh_bookOptions && in_array('show', $mh_bookOptions)): ?>
				<div class="mh-minimodule-book">
					<?php if( $mh_bookImage ): ?>
						<div class="mh-minimodule-book-image">
							<?php if( $mh_bookOptions && in_array('threed', $mh_bookOptions)) { ?>
							<div class="bookcover">
								<div class="bookcover-inner">
									<?php echo wp_get_attachment_image( $mh_bookImage, 'medium' ); ?>
								</div>
							</div>
							<?php } else { ?>
								<?php echo wp_get_attachment_image( $mh_bookImage, 'medium' ); ?>
							<?php } ?>
						</div>
					<?php endif; ?>
					<div class="mh-minimodule-book-text">
						<?php if( $mh_bookTitle ): ?>
							<h3 class="mh-minimodule-book-text-title"><?php echo $mh_bookTitle; ?></h3>
						<?php endif; ?>
						<?php if( $mh_bookDescription ): ?>
							<div class="mh-minimodule-book-text-desc">
								<?php echo $mh_bookDescription; ?>
							</div>
						<?php endif; ?>
						<?php if( $mh_bookButtonLink ): ?>
							<a href="<?php echo $mh_bookButtonLink; ?>" <?php if( $mh_bookButtonLinkType == 'external' ): ?> target="_blank" <?php endif ?> class="mh-button mh-minimodule-book-action" style="background-color: <?php echo $mh_heroTitleColor; ?>"><?php echo $mh_bookButtonLabel; ?></a>
						<?php endif; ?>
					</div>
				</div>
			<?php endif; ?>
		<?php endwhile; ?>
		</div>
	</section>
<?php endif; ?>

<!-- Bookclub Module -->
<?php
	$mh_bookclubTitle          = get_field('bookclubTitle');
	$mh_bookclubSubtitle       = get_field('bookclubSubtitle');
	$mh_bookclubFormShortcode  = get_field('bookclubFormShortcode');
	$mh_bookclubDescription    = get_field('bookclubDescription');
	$mh_bookclubBackground     = get_field('bookclubBackground');
	$mh_bookclubBGColor        = $mh_bookclubBackground['bookclubBackgroundColor'];
	$mh_bookclubBGImage        = $mh_bookclubBackground['bookclubBackgroundImage'];
	$mh_bookclubBGStyle        = "background-color: " . $mh_bookclubBGColor;
	$mh_bookclubBGStyle        = "; background-image: url(" . $mh_bookclubBGImage['url'] . ");";
?>
<section class="mh-module-bookclub" style="<?php echo $mh_bookclubBGStyle; ?>">
	<?php if( $mh_bookclubTitle ): ?>
		<h2 class="mh-module-bookclub-header">
			<span class="mh-module-bookclub-subtitle"><?php echo $mh_bookclubSubtitle; ?></span>
			<span class="mh-module-bookclub-title" style="color: <?php echo $mh_heroTitleColor; ?>"><?php echo $mh_bookclubTitle; ?></span>
		</h2>
	<?php endif; ?>
	<?php if( $mh_bookclubFormShortcode ): ?>
		<div class="mh-module-bookclub-form">
			<?php echo do_shortcode($mh_bookclubFormShortcode); ?>
		</div>
	<?php endif; ?>
	<?php if( $mh_bookclubDescription ): ?>
		<div class="mh-module-bookclub-description">
			<?php echo $mh_bookclubDescription; ?>
		</div>
	<?php endif; ?>
	<div class="mh-module-overlay" style="background-color: <?php echo $mh_bookclubBGColor; ?>;"></div>
</section>

<!-- Blog Module -->
<?php
	$mh_bloglistShowImages = get_field('bloglistShowImages');
	$mh_bloglistPostCount  = get_field('bloglistPostCount');
	$custom_query = new WP_Query('posts_per_page=' . $mh_bloglistPostCount);
?>
<section class="mh-module-blog">
	<h2 class="mh-module-blog-title">From the Blog</h2>
	<div class="mh-module-blog-excerpts">
		<?php while($custom_query->have_posts()) : $custom_query->the_post(); ?>
			<article class="mh-minimodule-excerpt" id="post-<?php the_ID(); ?>">
				<p class="mh-minimodule-excerpt-date"><?php printf( _x( '%s ago', '%s = human-readable time difference', 'make' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?>
				<h3 class="mh-minimodule-excerpt-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
				<?php if( $mh_bloglistShowImages && in_array('enabled', $mh_bloglistShowImages) ): ?>
				<div class="mh-minimodule-excerpt-image">
					<?php
						$imageArgs = array(
							'size' => 'excerpt-medium',
						);
						get_the_image($imageArgs);
					?>
				</div>
				<?php endif; ?>
				<div class="mh-minimodule-excerpt-text">
					<p><?php echo get_the_excerpt(); ?>&nbsp;[&hellip;]</p>
					<p><a class="mh-minimodule-excerpt-text-action" href="<?php the_permalink(); ?>">Read more</a></p>
				</div>
			</article>
		<?php endwhile; ?>
	</div>
</section>
<?php wp_reset_postdata(); ?>


<!-- About Module -->
<?php
	$mh_aboutTitle       = get_field('aboutTitle');
	$mh_aboutDescription = get_field('aboutDescription');
	$mh_aboutImage       = get_field('aboutImage');
	$mh_aboutButtonText  = get_field('aboutButtonText');
	$mh_aboutButtonLink  = get_field('aboutButtonLink');
?>
<section class="mh-module-about" style="<?php echo $mh_heroBGStyle; ?>">
	<?php if( $mh_aboutImage ): ?>
		<div class="mh-module-about-image">
			<?php echo wp_get_attachment_image( $mh_aboutImage, 'medium' ); ?>
		</div>
	<?php endif; ?>
	<?php if(( $mh_aboutTitle ) || ( $mh_aboutDescription ) || ( $mh_aboutButtonLink )): ?>
		<div class="mh-module-about-text">
			<?php if( $mh_aboutTitle ): ?>
				<h3 class="mh-module-about-text-title" style="color: <?php echo $mh_heroTitleColor; ?>"><?php echo $mh_aboutTitle; ?></h3>
			<?php endif; ?>
			<?php if( $mh_aboutDescription ): ?>
				<div class="mh-module-about-text-description">
					<?php echo $mh_aboutDescription; ?>
				</div>
			<?php endif; ?>
			<?php if( $mh_aboutButtonLink ): ?>
				<a href="<?php echo $mh_aboutButtonLink; ?>" class="mh-module-about-action" style="color: <?php echo $mh_heroTitleColor; ?>"><?php echo $mh_aboutButtonText; ?></a>
			<?php endif; ?>
		</div>
	<?php endif; ?>
	<div class="mh-module-overlay" style="background-color: <?php echo $mh_heroBGColor; ?>;"></div>
</section>

<?php get_footer(); ?>