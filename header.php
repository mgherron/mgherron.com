<?php
/**
 * @package Make
 */
?><!DOCTYPE html>
<!--[if lte IE 9]><html class="no-js IE9 IE" <?php language_attributes(); ?>><![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
	<head>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <?php
      if(is_post_type_archive('book')){
    ?>
      <meta property="og:image" content="https://mgherron.com/wp-content/uploads/2018/11/books-social-share.jpg" />
      <meta property="og:image:secure_url" content="https://mgherron.com/wp-content/uploads/2018/11/books-social-share.jpg" />
      <meta property="og:image:width" content="640" />
      <meta property="og:image:height" content="1024" />
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:image" content="https://mgherron.com/wp-content/uploads/2018/11/books-social-share.jpg" />
    <?php
      }
    ?>

    <?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?>>
		<div id="site-wrapper" class="site-wrapper">
			<a class="skip-link screen-reader-text" href="#site-content"><?php esc_html_e( 'Skip to content', 'make' ); ?></a>

			<?php ttfmake_maybe_show_site_region( 'header' ); ?>

			<div id="site-content" class="site-content">
				<div class="container">