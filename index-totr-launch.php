<?php
/**
 * @package Make
 */

get_header();

// Section Header
ob_start();
make_breadcrumb();
$section_header = trim( ob_get_clean() );

global $post;
?>

<?php ttfmake_maybe_show_sidebar( 'left' ); ?>

<section id="builder-section-1496002561267" class="builder-section builder-section-first builder-section-text builder-section-next-postlist builder-text-columns-2 has-background" style="background-image: url('http://mgherron.com/wp-content/uploads/2017/05/lost-places-1928727_960_720.jpg');background-size: cover; background-repeat: no-repeat;background-position: center center;">
		<div class="builder-section-content">
											<div class="builder-text-row">
						<div class="builder-text-column builder-text-column-1" id="builder-section-1496002561267-column-1">
								<div class="builder-text-content">
					<p><img class="aligncenter wp-image-5013 size-full" src="http://mgherron.com/wp-content/uploads/2017/05/totr-3d-transparent-square.png" alt="" width="1000" height="1000" srcset="http://mgherron.com/wp-content/uploads/2017/05/totr-3d-transparent-square.png 1000w, http://mgherron.com/wp-content/uploads/2017/05/totr-3d-transparent-square-150x150.png 150w, http://mgherron.com/wp-content/uploads/2017/05/totr-3d-transparent-square-150x150@2x.png 300w, http://mgherron.com/wp-content/uploads/2017/05/totr-3d-transparent-square-300x300@2x.png 600w" sizes="(max-width: 1000px) 100vw, 1000px"></p>
				</div>
							</div>
											<div class="builder-text-column builder-text-column-2" id="builder-section-1496002561267-column-2">
								<div class="builder-text-content">
					<h3 style="text-align: center;">&nbsp;</h3>
<h3 style="text-align: center;">&nbsp;<span style="color: #ffffff; text-shadow: 2px 2px #000000; font-size: 60px;">99¢ Sale!</span></h3>
<p style="text-align: center;"><span style="color: #ffffff;">Only for a limited time (June 13 – 22, 2017)</span></p>
<p style="text-align: center;"><a id="ttfmake-1496002806" class="ttfmake-button" style="background-color: #a2203d; color: #ffffff; font-size: 24px; font-weight: bold; padding: 10px; border-radius: 4px;" href="http://smarturl.it/totrbook" data-hover-background-color="#d93659" data-hover-color="#ffffff">Get the Book + Bonus Art!</a><br> <img class="aligncenter wp-image-5009" src="http://mgherron.com/wp-content/uploads/2017/05/Amazon-transparent-white-300x188.png" alt="" width="160" height="100" srcset="http://mgherron.com/wp-content/uploads/2017/05/Amazon-transparent-white-300x188.png 300w, http://mgherron.com/wp-content/uploads/2017/05/Amazon-transparent-white.png 1024w, http://mgherron.com/wp-content/uploads/2017/05/Amazon-transparent-white-300x188@2x.png 600w" sizes="(max-width: 160px) 100vw, 160px"></p>
				</div>
							</div>
							</div>
										</div>
		<div class="builder-section-overlay"></div>
	</section>

<main id="site-main" class="site-main" role="main">


<?php if ( have_posts() ) : ?>

	<?php if ( $section_header ) : ?>
	<header class="section-header">
		<?php echo $section_header; ?>
	</header>
	<?php endif; ?>

	<?php while ( have_posts() ) : the_post(); ?>
		<?php
		/**
		 * Allow for changing the template partial.
		 *
		 * @since 1.2.3.
		 *
		 * @param string     $type    The default template type to use.
		 * @param WP_Post    $post    The post object for the current post.
		 */
		$template_type = apply_filters( 'make_template_content_archive', 'archive', $post );
		get_template_part( 'partials/content', $template_type ); ?>
	<?php endwhile; ?>

	<?php get_template_part( 'partials/nav', 'paging' ); ?>

<?php else : ?>
	<?php get_template_part( 'partials/content', 'none' ); ?>
<?php endif; ?>
</main>

<?php ttfmake_maybe_show_sidebar( 'right' ); ?>

<?php get_footer(); ?>
