<?php
/* Archive sidebar */
?>
<section id="sidebar-right" class="widget-area sidebar sidebar-archive active" role="complementary">
  <div class="about-author-image">
    <img src="<?= get_field('bio_photo', 'option'); ?>" class="attachment-medium size-medium" alt="M. G. Herron, fantasy science fiction author">
  </div>
  <h3 class="about-author-headline"><?= get_field('bio_headline', 'option'); ?></h3>
  <p class="about-author-copy">
    <?= get_field('bio_copy', 'option'); ?>
  </p>
  <p class="center-text">
    <a href="/about" class="about-link book-btn info-button">Find out more</a>
  </p>


  <h3 class='search-heading'>Search</h3>
  <form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div style="display: flex">
      <input style="width: 70%" type="search" class="slick-search search-field left-border-radius" id="search-field" placeholder="<?php echo make_get_thememod_value( 'label-search-field' ); ?>" title="<?php esc_attr_e( 'Press Enter to submit your search', 'make' ) ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s" aria-label="<?php /* Translators: this string is a label for a search input that is only visible to screen readers. */ esc_html_e( 'Search for:', 'make' ); ?>">
      <input style="width: 30%" type="submit" class="slick-search search-submit right-border-radius" value="<?php /* Translators: this string is the label on the search submit button. */ esc_attr_e( 'Search', 'make' ); ?>" aria-label="<?php /* Translators: this string is the label on the search submit button for screen readers. */ esc_attr_e( 'Search', 'make' ); ?>" role="button">
    </div>
  </form>


  <!-- SERIES -->
  <h3 class='series-heading'>Series</h3>
  <ul class="series-list">
		<?php
			$terms = get_terms( 'series', array('hide_empty' => false));
			foreach($terms as $term){
				echo "<li><a href='/series/{$term->slug}'>{$term->name}</a></li>";
			}
		?>
  </ul>
  <br />

</section>