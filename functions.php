<?php
/**
 * @package Make Child
 */

/**
 * The theme version.
 */
define( 'TTFMAKE_CHILD_VERSION', '1.1.0' );

/**
 * Turn off the parent theme styles.
 *
 * If you would like to use this child theme to style Make from scratch, rather
 * than simply overriding specific style rules, simply remove the '//' from the
 * 'add_filter' line below. This will tell the theme not to enqueue the parent
 * stylesheet along with the child one.
 */
//add_filter( 'make_enqueue_parent_stylesheet', '__return_false' );

/**
 * Add your custom theme functions here.
 */

/*****************************************************************/
/* Custom Homepage using ACF
/*****************************************************************/

add_image_size( 'excerpt-medium', 640, 9999 ); //300 pixels wide (and unlimited height)
require_once( get_stylesheet_directory() . '/get-the-image.php' );

//echo get_template_directory();

// M.Hill 2018-04-04
// ACF is supposed to enable the ability to show/hide the content editor on selected templates.
// In practice I could not get this to work: it may be a conflict with Make or some other plugin.
// Given the tight turnaround for this work, I've brute forced the removal of the content editor on my
// specific template using this function:
// https://wordpress.stackexchange.com/questions/17367/hide-page-visual-editor-if-certain-template-is-selected
add_action( 'init', 'remove_editor_init' );
function remove_editor_init() {
    if ( ! is_admin() ) {
       return;
    }
    $current_post_id = filter_input( INPUT_GET, 'post', FILTER_SANITIZE_NUMBER_INT );
    $update_post_id = filter_input( INPUT_POST, 'post_ID', FILTER_SANITIZE_NUMBER_INT );

    // Check to see if the post ID is set, else return.
    if ( isset( $current_post_id ) ) {
       $post_id = absint( $current_post_id );
    } else if ( isset( $update_post_id ) ) {
       $post_id = absint( $update_post_id );
    } else {
       return;
    }

    // Don't do anything unless there is a post_id.
    if ( isset( $post_id ) ) {
       // Get the template of the current post.
       $template_file = get_post_meta( $post_id, '_wp_page_template', true );

       // Example of removing page editor for page-your-template.php template.
       if (  'homepage-acf.php' === $template_file ) {
           remove_post_type_support( 'page', 'editor' );
           // Other features can also be removed in addition to the editor. See: https://codex.wordpress.org/Function_Reference/remove_post_type_support.
       }
    }
}

add_filter('excerpt_more', 'change_excerpt_more', 100);
function change_excerpt_more($more){
    return '';
}


if( function_exists('acf_add_options_page') ) {

	acf_add_options_page();

}

function books_change_posts_per_page( $query ) {
	if ( is_admin() || ! $query->is_main_query() ) {
		return;
	}

	if ( is_post_type_archive( 'book' ) ) {
		$query->set( 'posts_per_page', 999 );
	}
}
add_filter( 'pre_get_posts', 'books_change_posts_per_page' );