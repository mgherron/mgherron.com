<?php
/**
 * @package Make
 */

// Header Options
$subheader_class = ( make_get_thememod_value( 'header-show-social' ) ) ? ' right-content' : '';
$mobile_menu = make_get_thememod_value( 'mobile-menu' );
$header_menu_container_class = 'header-bar-menu' . ( 'header-bar' === $mobile_menu ? ' mobile-menu': ' desktop-menu' );

$header_bar_menu = wp_nav_menu( array(
	'theme_location'  => 'header-bar',
	'container_class' => $header_menu_container_class,
	'depth'           => 1,
	'fallback_cb'     => false,
	'echo'            => false,
) );

set_query_var( 'mobile_menu', $mobile_menu );
set_query_var( 'header_bar_menu', $header_bar_menu );

// M.Hill 2018-04-10-1230
// Get the ID of the homepage using the custom ACF template

// $mh_homepage_args = [
//     'post_type' => 'page',
//     'fields' => 'ids',
//     'nopaging' => true,
//     'meta_key' => '_wp_page_template',
//     'meta_value' => 'homepage-acf.php'
// ];
// $mh_homepage   = get_posts( $mh_homepage_args );
// $mh_homepageID = $mh_homepage[0];

// M.Hill 2018-04-10-1230
// Get the ACF variables from the custom homepage hero module,
// and use them to style the site header with the same background

// $mh_heroTitle      = get_field('heroTitle', $mh_homepageID);
// $mh_heroTitleColor = $mh_heroTitle['heroTitleColor'];
// $mh_heroBackground = get_field('heroBackground', $mh_homepageID);
// $mh_heroBGImage    = $mh_heroBackground['heroBackgroundImage'];
// $mh_heroBGStyle    = "; background-image: url(" . $mh_heroBGImage['url'] . ");";

?>

<header id="site-header" class="<?php echo esc_attr( ttfmake_get_site_header_class() ); ?> <?php //echo $mh_homepageID; ?>" role="banner" style="<?php //echo $mh_heroBGStyle; ?>">
	<div class="site-header-main">
		<div class="container">
			<div class="site-branding">
				<?php // Logo
				if ( make_has_logo() ) : ?>
					<?php make_logo(); ?>
				<?php endif; ?>
				<?php // Site title
				if ( get_bloginfo( 'name' ) ) : ?>
				<h1 class="site-title<?php if ( make_get_thememod_value( 'hide-site-title' ) ) echo ' screen-reader-text'; ?>">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
				</h1>
				<?php endif; ?>
				<?php // Tagline
				if ( get_bloginfo( 'description' ) ) : ?>
				<span class="site-description<?php if ( make_get_thememod_value( 'hide-tagline' ) ) echo ' screen-reader-text'; ?>">
					<?php bloginfo( 'description' ); ?>
				</span>
				<?php endif; ?>
			</div>

			<div class="site-nav-search">

			<?php // Social links
				// if ( make_has_socialicons() && make_get_thememod_value( 'header-show-social' ) ) {
				//	make_socialicons( 'header' );
				// }
			?>

			<?php // Navigation
				get_template_part( 'partials/nav', 'header-main' ); ?>

			<?php // Search form
			if ( make_get_thememod_value( 'header-show-search' ) ) :
				get_search_form();
			endif; ?>

			</div>

		</div>
	</div>
</header>