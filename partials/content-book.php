<?php
	/**
	 * @package Make
	 */

	$thumb_key    = 'layout-' . make_get_current_view() . '-featured-images';
	$thumb_option = make_get_thememod_value( $thumb_key );
	$series = wp_get_object_terms( get_the_ID(), array( 'genre' ) );
	$filter_class = $terms[0]->slug;
	$amazon_link = get_field('amazon_link');
	$more_info_link = get_field('more_info_link');
	$cover_link = ($more_info_link) ? $more_info_link : $amazon_link;

	// Header
	ob_start();
	//get_template_part( 'partials/entry', 'meta-top' );
	//get_template_part( 'partials/entry', 'sticky' );
	//get_template_part( 'partials/entry', 'thumbnail' );
	//get_template_part( 'partials/entry', 'title' );
	//get_template_part( 'partials/entry', 'meta-before-content' );
	//$thumbnail_size = make_get_entry_thumbnail_size( $thumb_option );
	
	$thumbnail_html = get_the_post_thumbnail( get_the_ID(), 'medium' );
	echo '<figure class="entry-thumbnail">';
	echo '<a class="more-link" target="_blank" href="'.$cover_link.'" rel="bookmark">';
	echo $thumbnail_html;
	echo '</a>';
	echo '</figure>';
	$entry_header = trim( ob_get_clean() );

	// Footer
	ob_start();
	//get_template_part( 'partials/entry', 'meta-post-footer' );
	//get_template_part( 'partials/entry', 'taxonomy' );

	echo '<a target="_blank" href="'.$amazon_link.'" class="book-btn buy-button">Buy Now</a>';

	get_template_part( 'partials/entry', 'sharing' );
	$entry_footer = trim( ob_get_clean() );


	//  var_dump($series);
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(array('book', $filter_class)); ?>>
	<?php
		$three_dimensional = get_field( "3d_effect" );
	?>
	<?php if ( $entry_header ) : ?>
    <header class="entry-header">
			<?php if($three_dimensional [0]=="Yes"): ?>
      <div class="bookcover">
        <div class="bookcover-inner">
					<?php endif; ?>
					<?php echo $entry_header; ?>
					<?php if($three_dimensional [0]=="Yes"): ?>
        </div>
      </div>
		<?php endif; ?>
    </header>
	<?php endif; ?>
  <footer class="entry-footer">
	  <?php if($more_info_link): ?>
      <a class="book-btn info-button" href="<?= $more_info_link ?>" rel="bookmark">More Info</a>
	  <?php endif; ?>
      <a class="book-btn buy-button" target="_blank" href="<?= $amazon_link; ?>">Buy Now</a>
  </footer>
</article>
