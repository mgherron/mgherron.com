<?php
get_header();
global $post;
?>
<style>
  .site-content {
    padding: .5rem 0!important;
  }

  .container{
    max-width: 1400px;
    margin: 0 auto;
    padding: 1rem;
  }

  .section-header {
   margin-bottom: 10px;
  }

  article.one-fourth {
    width: 20%;
    /*margin: 0 2.5%;*/
    margin: 2.5%;
    float: left;
  }
/* filters */
  ul.genre-filter {
    margin: 10px 0!important;
    padding: 0;
    }

  ul.genre-filter li {
    display: inline-grid;
    margin-bottom: 5px;
  }

  ul.genre-filter li a{
    transition: .25s all ease-out;
    background: transparent;
    color: #1f4889;
    border: 2px solid #1f4889;
    padding: 4px 8px;
    margin: 0 10px 0 0;
    border-radius: 12px;
    text-decoration: none;
    }

  ul.genre-filter li a:hover{
    /*background: #1f4889;*/
    /*color: #fff;*/
    border: 2px solid #1f4889;
    text-decoration: underline;
    }
  ul.genre-filter li a.active{
    background: #1f4889;
    color: #fff;
  }

  label.genre-filter-label {
    color: #1F4889;
    font-family: Oswald, Roboto, sans-serif !important;
  }

  /* buy button */
  a.buy-button {
    width: 100%;
    background: #f2ad58;
    padding: 12px 0;
    display: inline-block;
    border-radius: 100px;
    text-transform: uppercase;
    text-align: center;
    line-height: 1;
    font-style: 600 !important;
    font-family: Oswald, Roboto, sans-serif !important;
    color: #0c152d;
  }

  a.buy-button:hover {
    color: #0c152d;
    text-decoration: none;
  }

  .entry-header {
    margin: 0;
  }
  .entry-header .entry-thumbnail {
    margin: 11px 0;
  }
  /* border between main / sidebar */
  main#site-main {
    border-right: 1px dashed #ccc;
  }

  /* reduce margin under breadcrumbs */
  .yoast-seo-breadcrumb {
    margin-bottom: 1em;
  }

  #sidebar-right {
    /*float: left;*/
    width: 100%;
  }

  /* responsive styling */
  @media screen and (min-width: 800px){
    .has-right-sidebar .site-main {
      width: 74%!important;
    }
    .has-right-sidebar #sidebar-right{
      width: 24%!important;
    }
  }

  @media screen and (max-width: 799px){
    article.one-fourth {
      /*width: 100%;*/
      /*margin: 2%;*/
      /*float: none;*/
      width: 46%;
      margin: 2%;
      float: left;
    }
    /* mobile filters */
    ul.genre-filter li {
      display: grid;
      text-align: center;
      margin-bottom: 1%;
    }
  } /* end media query */
</style>

<main id="site-main" class="site-main" role="main">
  <?php if ( have_posts() ) : ?>

    <!-- HEADER -->
    <header class="section-header">
      <?php make_breadcrumb() ?>
      <?php //get_template_part( 'partials/section', 'title' ); ?>
      <label class='genre-filter-label'><small>FILTER BY GENRE</small></label><br />
      <ul class="genre-filter">
      <?php
        $terms = get_terms( 'genre', array('hide_empty' => false));
        foreach($terms as $term){
          echo "<li><a class='filter-button' href='#{$term->slug}'>{$term->name}</a></li>";
        }
      ?>
      </ul>
      <input id="selected-filters" type="hidden">
    </header>

    <!-- THE LOOP -->
    <?php
    $count = 0;
    while ( have_posts() ) : the_post(); ?>
      <?php
	      // if 3rd, clearfix
//        if($count % 3 == 0){ echo "<div style=''clear:both'></div>"; }

	      get_template_part( 'partials/content', 'book' );

      ?>
    <?php
    $count++;
    endwhile; ?>

    <!-- PAGINATION -->
    <?php get_template_part( 'partials/nav', 'paging' ); ?>

  <?php else : ?>
    <?php get_template_part( 'partials/content', 'none' ); ?>
  <?php endif; ?>
</main>

<?php get_sidebar( 'archive' ); ?>
<script>
  jQuery(document).ready(function($){

    // on filter button click
    $('body').on('click','.filter-button', function(){
      event.preventDefault();
      var filter = this.href.split('#');
      var filter = filter.pop(1);

      if(!$(this).hasClass('active')){ // If not "active", then add .aciveclass
        $(this).addClass('active');
        $('#selected-filters').val(function(index, value) { // add filter to field for reference later
          return value + '.genre-' +filter + ',' ;
        });
      } else {
        $(this).removeClass('active');
        $('#selected-filters').val(function(index, value) {
          return value.replace('.genre-'+filter+',','');
        });
      }


      filterBooks();
//      $('article').hide();
//      $('article.genre-'+filter).fadeIn('fast');

    })

    function filterBooks(){
      var selectors = jQuery("#selected-filters").val();
      selectors = selectors.substring(0, selectors.length - 1)
      if(selectors!=''){
        $('article').hide();
        $(selectors).fadeIn('fast');
      } else {
        $('article').fadeIn('fast');
      }
//      console.log(selectors);
    }

  });
</script>
<?php get_footer(); ?>
