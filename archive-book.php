<?php
	get_header();
	global $post;
?>
<link href="<?= get_stylesheet_directory_uri(); ?>/archive-style.css" rel="stylesheet">
<main id="site-main" class="site-main" role="main">
	<?php if ( have_posts() ) : ?>

    <!-- HEADER -->
    <header class="section-header">
      <!--      --><?php //make_breadcrumb() ?>
			<?php //get_template_part( 'partials/section', 'title' ); ?>
      <label class='genre-filter-label'><small>FILTER BY GENRE</small></label><br />
      <ul class="genre-filter">
				<?php
					$terms = get_terms( 'genre', array('hide_empty' => false));
					foreach($terms as $term){
						echo "<li><a class='filter-button' href='#{$term->slug}'>{$term->name}</a></li>";
					}
				?>
      </ul>
      <input id="selected-filters" type="hidden">
    </header>

    <div class="book-grid">
      <!-- THE LOOP -->
			<?php
				$count = 0;
				while ( have_posts() ) : the_post(); ?>
					<?php
					// if 3rd, clearfix
//        if($count % 3 == 0){ echo "<div style=''clear:both'></div>"; }

					get_template_part( 'partials/content', 'book' );

					?>
					<?php
					$count++;
				endwhile; ?>
    </div>
	<?php else : ?>
		<?php get_template_part( 'partials/content', 'none' ); ?>
	<?php endif; ?>
</main>

<?php get_sidebar( 'archive' ); ?>
<script>
  jQuery(document).ready(function($){

    // on filter button click
    $('body').on('click','.filter-button', function(event){
      event.preventDefault();
      var filter = this.href.split('#');
      var filter = filter.pop(1);

      if(!$(this).hasClass('active')){ // If not "active", then add .aciveclass
        $(this).addClass('active');
        $('#selected-filters').val(function(index, value) { // add filter to field for reference later
          return value + '.genre-' +filter + ',' ;
        });
      } else {
        $(this).removeClass('active');
        $('#selected-filters').val(function(index, value) {
          return value.replace('.genre-'+filter+',','');
        });
      }


      filterBooks();
//      $('article').hide();
//      $('article.genre-'+filter).fadeIn('fast');

    })

    function filterBooks(){
      var selectors = jQuery("#selected-filters").val();
      selectors = selectors.substring(0, selectors.length - 1)
      if(selectors!=''){
        $('article').hide();
        $(selectors).fadeIn('fast');
      } else {
        $('article').fadeIn('fast');
      }
//      console.log(selectors);
    }

  });
</script>
<?php get_footer(); ?>